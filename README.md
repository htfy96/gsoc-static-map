# Static Map

[![build status](https://gitlab.com/htfy96/gsoc-static-map/badges/master/build.svg)](https://gitlab.com/htfy96/gsoc-static-map/commits/master)
[![Project Status: WIP - Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.](http://www.repostatus.org/badges/latest/wip.svg)](http://www.repostatus.org/#wip)

A compile-time map built with `constexpr` targeting C++ 14 and zero runtime overhead.

This is a implementation of [GSoC2017 project of Boost Foundation](https://svn.boost.org/trac/boost/wiki/SoC2017).

## Highlights
- O(1) lookup
- string literals supported by `constexpr_string_view`
- Customizable constexpr comparator and hash function
- Modifiable

## Tested platform
- g++5.3
- g++6
- clang++3.8
- clang++3.9

